class UserDeal < ActiveRecord::Base

  belongs_to :user
  belongs_to :deal

  after_create :decrement_deal

  def decrement_deal
    self.deal.update_columns(quantity: self.deal.quantity - 1)
  end

end
